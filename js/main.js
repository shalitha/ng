var app = angular.module("MyApp", []);

app.controller('MainCtrl', function($scope, Student){
	$scope.list = Student.getAll();

	$scope.save = function(){
		$scope.list.push($scope.text);
	}

	$scope.edit = function(id){
		$scope.student = Student.get(id);
	}
});


app.service('Student', function(){
	var data = {
		1: { name: "Kamal Perara", age: 21, id: 1 },
		2: { name: "Nimal Perara", age: 20, id: 2 },
		3: { name: "Amal Silva", age: 23, id: 3 }
	};

	var studentService = {};

	studentService.getAll = function(){
		return data;
	};

	studentService.get = function(id){
		return data[id];
	}

	return studentService;
});